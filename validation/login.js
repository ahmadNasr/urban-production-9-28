const Validator = require('validator');
const isEmpty = require('./is-empty');

module.exports = function validateLoginInput(data) {
  let errors = {};

  data.identifier = !isEmpty(data.identifier) ? data.identifier : '';
  data.password = !isEmpty(data.password) ? data.password : '';

  if (!Validator.isEmail(data.identifier))
    errors.identifier = 'Email field is invalid';

  if (!Validator.isLength(data.password, { min: 6, max: 30 }))
    errors.password = 'Password must be at least 6 characters';

  if (Validator.isEmpty(data.identifier))
    errors.identifier = 'Email field is required';

  if (Validator.isEmpty(data.password))
    errors.password = 'Password field is required';

  return {
    errors,
    isValid: isEmpty(errors)
  };
};
