import React, { Component } from 'react';
import { withTranslation } from 'react-i18next';
import { MDBBtn, MDBIcon, Row, Col, Container } from 'mdbreact';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import {
  saveSelectValue,
  setCurrentCityError
} from '../../../actions/selectActions';
import {
  ComposableMap,
  ZoomableGroup,
  Geographies,
  Geography,
  Annotations,
  Annotation,
  Markers,
  Marker
} from 'react-simple-maps';
import Select from 'react-select';
import { Motion, spring } from 'react-motion';
import ReactTooltip from 'react-tooltip';
import admin1, { objects as annotations } from '../../../json/admin1.json';
import {
  groupedOptions,
  groupedOptionsAr,
  options,
  govLabelCoordinates
} from '../../../json/data.js';

const wrapperStyles = {
  width: '100%',
  margin: '0 auto'
};

class CityProfilesMap extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedOption: null,
      center: [39, 34.9],
      zoom: 5,
      districtSelected: false,
      citySelected: false,
      districtPcode: null
    };
  }

  componentDidMount() {
    setTimeout(() => {
      ReactTooltip.rebuild();
    }, 100);
  }

  populateSelectValue = selectedOption => {
    this.props.saveSelectValue({
      name: selectedOption.label,
      pcode: selectedOption.admin4Pcode
    });
  };

  handleZoomIn = () => {
    this.setState({
      zoom: this.state.zoom * 2
    });
  };

  handleZoomOut = () => {
    this.setState({
      zoom: this.state.zoom / 2
    });
  };

  handleReset = () => {
    this.populateSelectValue({ label: [], admin4Pcode: [] });
    this.setState({
      selectedOption: null,
      center: [39, 34.9],
      zoom: 5,
      citySelected: false,
      districtSelected: false,
      districtPcode: null
    });
    this.props.setCurrentCityError(
      'Choose a city, or click on a governorate to show available products'
    );
  };

  handleAdminLevelClick = (adminType, districtPcode = null, city = null) => (
    marker = null
  ) => {
    if (adminType === 'city') {
      if (city && city.admin4Pcode && marker && marker.coordinates) {
        this.setState({
          citySelected: true,
          districtSelected: false,
          districtPcode: null
        });
        const cityClicked = { label: [], admin4Pcode: [] };
        cityClicked.label.push(city.label);
        cityClicked.admin4Pcode.push(city.admin4Pcode);

        this.populateSelectValue({
          label: cityClicked.label,
          admin4Pcode: cityClicked.admin4Pcode
        });

        const selectedCityClick = {
          value: marker.coordinates,
          label: this.props.t(`urban-cities.${city.label}`)
        };

        this.setState({
          selectedOption: selectedCityClick,
          center: marker.coordinates,
          zoom: 25
        });
      }
      else {
        console.log('Error! No pcode/marker found for the selected city!');
      }
    }
    else {
      if (districtPcode) {
        this.setState({
          selectedOption: null,
          center: [39, 34.9],
          zoom: 5,
          districtSelected: true,
          citySelected: false,
          districtPcode: districtPcode
        });
        const districtExists = options.some(
          option => option['admin1Pcode'] === districtPcode
        );
        if (districtExists) {
          const citiesOfDistrict = { label: [], admin4Pcode: [] };
          for (let i = 0; i < options.length; i++) {
            if (districtPcode === options[i].admin1Pcode) {
              citiesOfDistrict.label.push(options[i].label);
              citiesOfDistrict.admin4Pcode.push(options[i].admin4Pcode);
            }
          }
          this.populateSelectValue({
            label: citiesOfDistrict.label,
            admin4Pcode: citiesOfDistrict.admin4Pcode
          });
        }
        else {
          this.populateSelectValue({ label: [], admin4Pcode: [] });
        }
      }
      else {
        console.log('Error! No Pcode found for the selected governorate!');
      }
    }
  };

  handleChange = selectedOption => {
    if (selectedOption && selectedOption.value && selectedOption.label) {
      const cityClicked = { label: [], admin4Pcode: [] };
      cityClicked.label.push(selectedOption.label);
      cityClicked.admin4Pcode.push(selectedOption.admin4Pcode);
      this.populateSelectValue(cityClicked);
      this.setState({
        selectedOption,
        center: selectedOption.value,
        zoom: 25,
        citySelected: true,
        districtSelected: false,
        districtPcode: null
      });
    }
    else {
      this.populateSelectValue({ label: [], admin4Pcode: [] });
      this.setState({
        selectedOption: null,
        center: [39, 34.9],
        zoom: 5,
        citySelected: false,
        districtSelected: false,
        districtPcode: null
      });
      this.props.setCurrentCityError(
        'Choose a city, or click on a governorate to show available products'
      );
    }
  };

  render() {
    const { t } = this.props;
    const dot = (color = '#ccc') => ({
      alignItems: 'center',
      display: 'flex',
      ':before': {
        backgroundColor: color,
        borderRadius: 10,
        content: '" "',
        display: 'block',
        marginRight: 8,
        height: 10,
        width: 10
      }
    });
    const customStyles = {
      control: styles => ({ ...styles, backgroundColor: 'transparent' }),
      option: (styles, { data, isDisabled, isFocused, isSelected }) => {
        return {
          ...styles,
          backgroundColor: isDisabled
            ? null
            : isSelected ? '#ff8800' : isFocused ? '#ff8800' : null,

          cursor: isDisabled ? 'not-allowed' : 'default'
        };
      },
      input: styles => ({ ...styles, ...dot() }),
      placeholder: styles => ({ ...styles, ...dot() }),
      singleValue: styles => {
        return { ...styles, ...dot('#ff8800') };
      }
    };

    const formatGroupLabel = data => (
      <div className='d-flex flex-wrap align-items-center justify-content-between text-white'>
        <div className='d-flex align-items-center justify-content-start font-smaller flex-wrap'>
          <div
            className={`px-05 ${!t('language.isRTL') ? 'order-1' : 'order-2'}`}>
            {t(`syria-gov.${data.label}`)}
          </div>
          <div
            className={`px-05 ${!t('language.isRTL') ? 'order-2' : 'order-1'}`}>
            {t('city-profiles.map-section.governorate')}
          </div>
        </div>
        <div className='font-smaller px-05'>{data.options.length}</div>
      </div>
    );
    return (
      <Container fluid>
        <Row center>
          <Col sm='6' size='12'>
            <div className='text-center mt-5 mb-4'>
              <h3 className='h3-responsive pb-3'>
                {t('city-profiles.map-section.paragraph.title')}
              </h3>
              <p className='pb-3'>
                {t('city-profiles.map-section.paragraph.body')}
              </p>
            </div>
          </Col>

          <Col sm='6' size='12'>
            <div className='h-100 d-flex py-3 pt-sm-5 flex-column justify-content-center align-items-center'>
              <div
                className='w-65 p-2 font-small'
                dir={`${t('language.isRTL') && 'rtl'}`}>
                <Select
                  className={`btn-outline-rounded-urban-dark-blue font-smaller ${t(
                    'language.isRTL'
                  ) && 'text-right'} `}
                  id='urban-select'
                  value={this.state.selectedOption}
                  onChange={this.handleChange}
                  options={
                    !t('language.isRTL') ? groupedOptions : groupedOptionsAr
                  }
                  isSearchable
                  isClearable
                  placeholder={t('city-profiles.map-section.choose-city')}
                  styles={customStyles}
                  formatGroupLabel={formatGroupLabel}
                />
              </div>

              <div className='p-2 z-0'>
                <MDBBtn
                  className='reset-btn mx-3 urban-dark-orange font-weight-600'
                  size='sm'
                  onClick={this.handleReset}>
                  {t('city-profiles.map-section.reset-btn')}
                </MDBBtn>

                <MDBBtn
                  className='zoom-btn mx-3 urban-dark-blue'
                  floating
                  size='sm'
                  onClick={this.handleZoomOut}>
                  <MDBIcon icon='search-minus' className='zoom-btn-icon' />
                </MDBBtn>

                <MDBBtn
                  className='zoom-btn mx-3 urban-dark-blue'
                  floating
                  size='sm'
                  onClick={this.handleZoomIn}>
                  <MDBIcon icon='search-plus' className='zoom-btn-icon' />
                </MDBBtn>
              </div>
            </div>
          </Col>
        </Row>

        <Row center>
          <Col size='12' className='px-0 mb-2'>
            <div style={wrapperStyles} className='px-0 px-sm-5'>
              <Motion
                defaultStyle={{
                  zoom: 1,
                  x: 0,
                  y: 20
                }}
                style={{
                  zoom: spring(this.state.zoom, {
                    stiffness: 210,
                    damping: 20
                  }),
                  x: spring(this.state.center[0], {
                    stiffness: 210,
                    damping: 20
                  }),
                  y: spring(this.state.center[1], {
                    stiffness: 210,
                    damping: 20
                  })
                }}>
                {({ zoom, x, y }) => (
                  <ComposableMap
                    projectionConfig={{ scale: 1300 }}
                    width={980}
                    height={551}
                    style={{
                      width: '100%',
                      height: 'auto'
                    }}>
                    <ZoomableGroup center={[x, y]} zoom={zoom}>
                      <Geographies
                        geography={admin1}
                        disableOptimization={true}>
                        {(geographies, projection) =>
                          geographies.map((geography, i) => (
                            <Geography
                              className='cursor-pointer'
                              onClick={this.handleAdminLevelClick(
                                'district',
                                geography.properties.admin1Pcode
                              )}
                              key={i}
                              cacheId={geography.properties.admin1Pcode + i}
                              geography={geography}
                              projection={projection}
                              style={{
                                default: {
                                  fill:
                                    this.state.districtSelected &&
                                    this.state.districtPcode ===
                                      geography.properties.admin1Pcode
                                      ? '#BDBDBD'
                                      : '#ECEFF1',
                                  stroke: '#607D8B',
                                  strokeWidth: 0.25,
                                  outline: 'none',
                                  transition: 'all 100ms'
                                },
                                hover: {
                                  fill: '#CFD8DC',
                                  stroke: '#607D8B',
                                  strokeWidth: 0.35,
                                  outline: 'none',
                                  transition: 'all 100ms'
                                },
                                pressed: {
                                  fill: '#CFD8DC',
                                  stroke: '#607D8B',
                                  strokeWidth: 0.35,
                                  outline: 'none',
                                  transition: 'all 100ms'
                                }
                              }}
                            />
                          ))}
                      </Geographies>

                      <Annotations>
                        {annotations.ADMIN1.geometries.map((annotation, i) => (
                          <Annotation
                            key={i}
                            dx={0}
                            dy={0}
                            subject={govLabelCoordinates[i]}
                            strokeWidth={0.2}>
                            <text
                              className='cursor-pointer'
                              fontSize={2.55}
                              onClick={this.handleAdminLevelClick(
                                'district',
                                annotation.properties.admin1Pcode
                              )}>
                              {t(
                                `syria-gov.${annotation.properties
                                  .admin1Name_en}`
                              )}
                            </text>
                          </Annotation>
                        ))}
                      </Annotations>

                      <Markers>
                        {options.map((city, i) => (
                          <Marker
                            key={i}
                            marker={{ type: 'Point', coordinates: city.value }}
                            onClick={this.handleAdminLevelClick(
                              'city',
                              null,
                              city
                            )}>
                            <g
                              cursor='pointer'
                              data-tip={t(`urban-cities.${city.label}`)}
                              transform='translate(-12, -24)'>
                              <path
                                fill='#FF5722'
                                strokeWidth='2'
                                strokeLinecap='square'
                                strokeMiterlimit='10'
                                strokeLinejoin='miter'
                                d='M20,9c0,4.9-8,13-8,13S4,13.9,4,9c0-5.1,4.1-8,8-8S20,3.9,20,9z'
                              />
                              <circle
                                fill='#fefefe'
                                strokeWidth='2'
                                strokeLinecap='square'
                                strokeMiterlimit='10'
                                strokeLinejoin='miter'
                                cx='12'
                                cy='9'
                                r='3'
                              />
                            </g>

                            {this.state.selectedOption &&
                            this.state.selectedOption.label &&
                            this.props.t(
                              `urban-cities.${this.state.selectedOption.label}`
                            ) === this.props.t(`urban-cities.${city.label}`) ? (
                              <text
                                textAnchor='middle'
                                y={-40}
                                stroke='#fff'
                                strokeWidth={1.2}
                                fontSize={30}
                                fontWeight='bold'
                                fill='#FF5722'
                                fontFamily={
                                  !t('language.isRTL') ? (
                                    'Montserrat, sans-serif'
                                  ) : (
                                    'MontserratAr, sans-serif'
                                  )
                                }>
                                {t(`urban-cities.${city.label}`)}
                              </text>
                            ) : null}
                          </Marker>
                        ))}
                      </Markers>
                    </ZoomableGroup>
                  </ComposableMap>
                )}
              </Motion>
              <ReactTooltip multiline={true} clickable={true} />
            </div>
          </Col>
        </Row>
      </Container>
    );
  }
}

CityProfilesMap.propTypes = {
  saveSelectValue: PropTypes.func.isRequired,
  setCurrentCityError: PropTypes.func.isRequired,
  city: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  city: state.city
});

export default connect(mapStateToProps, {
  saveSelectValue,
  setCurrentCityError
})(withRouter(withTranslation()(CityProfilesMap)));
