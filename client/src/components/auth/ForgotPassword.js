import React from 'react';
import { withTranslation } from 'react-i18next';
import {
  MDBContainer,
  MDBBtn,
  MDBModal,
  MDBModalBody,
  MDBModalHeader,
  MDBIcon
} from 'mdbreact';
import { connectModal } from 'redux-modal';
import axios from 'axios';

class ForgotPasswordModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      success: false,
      loading: false,
      errors: {}
    };
  }

  onChange = e => {
    this.setState({
      ...this.state,
      [e.target.name]: e.target.value.toLowerCase(),
      success: false,
      errors: { ...this.state.errors, [e.target.name]: '' }
    });
  };

  onSubmit = e => {
    e.preventDefault();
    this.setState({ loading: true, success: false });
    const forgotPassswordData = {
      email: this.state.email
    };

    axios
      .post(
        `${process.env.REACT_APP_API_URL}/user/forgot-password`,
        forgotPassswordData
      )
      .then(res => {
        this.setState({ success: true, loading: false, errors: {} });
      })
      .catch(err => {
        this.setState({
          errors: err.response.data.message,
          loading: false,
          success: false
        });
      });
  };

  render() {
    const { show, handleHide, t } = this.props;
    const { errors, email, success, loading } = this.state;

    return (
      <MDBContainer
        className={`font-weight-normal ${!t('language.isRTL')
          ? 'text-left'
          : 'text-right'}`}
        dir={`${t('language.isRTL') && 'rtl'}`}>
        <MDBModal
          className='forgot-password form-cascading'
          isOpen={show}
          toggle={handleHide}
          centered>
          <MDBModalHeader
            className='align-items-center text-center text-white warning-color-dark darken-3'
            titleClass='w-100'
            tag='h5'
            toggle={handleHide}>
            <MDBIcon icon='lock' className='text-white fa-sm mx-2 font-small' />
            <span className='font-weight-normal font-small'>
              {t('forms.forgot-pwd')}
            </span>
          </MDBModalHeader>
          <MDBModalBody className='text-center d-flex flex-column align-items-center justify-content-center'>
            {success && (
              <div className='alert alert-success text-center font-small'>
                {t('forms.forgot-success')} {email}
              </div>
            )}

            <div className='px-3 pt-3 pb-0 mb-2 text-center font-smaller text-center font-smaller'>
              {t('forms.forgot-paragraph-p1')} <br />{' '}
              {t('forms.forgot-paragraph-p2')}
            </div>
            <form noValidate onSubmit={this.onSubmit} className='w-50'>
              <input
                type='email'
                className={`form-control form-control-sm text-lowercase ${errors.email &&
                  'is-invalid'}`}
                name='email'
                value={this.state.email.toLowerCase()}
                onChange={this.onChange}
              />

              {errors.email && (
                <div className='invalid-feedback'>
                  {t(`forms.${errors.email}`)}
                </div>
              )}
              <br />

              <div className='text-center'>
                <MDBBtn
                  type='submit'
                  className={`font-weight-500 btn-outline-urban-dark-orange btn-sm ${loading &&
                    'disabled'}`}>
                  {loading ? (
                    <div
                      className='spinner-grow text-warning spinner-grow-sm'
                      role='status'>
                      <span className='sr-only'>{t('forms.loading')}</span>
                    </div>
                  ) : (
                    <span>
                      {t('forms.send')}
                      <MDBIcon icon='paper-plane' className='mx-1' />
                    </span>
                  )}
                </MDBBtn>
              </div>
            </form>
          </MDBModalBody>
        </MDBModal>
      </MDBContainer>
    );
  }
}

export default connectModal({ name: 'forgotPasswordModal' })(
  withTranslation()(ForgotPasswordModal)
);
