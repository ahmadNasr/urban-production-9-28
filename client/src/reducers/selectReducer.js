import { SAVE_SELECT_OPTION } from '../actions/types';

const initialState = { name: [], pcode: [], products: [], errors: [] };

export default function(state = initialState, action) {
  switch (action.type) {
    case SAVE_SELECT_OPTION:
      return {
        ...state,
        name: action.payload.name,
        pcode: action.payload.pcode,
        products: action.payload.products,
        errors: action.payload.errors
      };

    default:
      return state;
  }
}
